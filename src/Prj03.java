import java.util.InputMismatchException;
import java.util.Scanner;

public class Prj03 {
    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)) {
            boolean check = true;
            while (check) {

                Integer first = scanner.nextInt();
                Integer second = scanner.nextInt();
                System.out.println("Сложение: " + (first + second));
                System.out.println("Вычитание: " + (first - second));
                System.out.println("Умножение: " + (first * second));

                if (second == 0) {
                    System.out.println("Деление на ноль запрещено");
                    System.out.println("Введите данные заново!");
                    continue;
                }
                System.out.println("Деление: " + (first / second));
                System.out.println("Остаток от деления: " + (first % second));
                System.out.println("Ввести данные заново?Y/N");
                String exit = scanner.next();
                if(exit.equals("N")) {
                    check = false;
                } else if (exit.equals("Y")) {
                    continue;
                }
                else {
                    System.out.println("Неверный ответ");
                    return;
                }
            }

        } catch (InputMismatchException e) {
            System.err.print("Ошибка ввода");
        }


    }
}
